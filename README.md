# AddMusic简介

## 功能简介
读取本地文件夹下所有音频文件，按照文件名'[歌名] [歌手]'添加到QQ音乐收藏（我喜欢歌单）。国语歌手效果佳。

## 运行环境
- Windows 10
- Python 3.10
- Selenium 4.0
- Chromedriver 111.0.5563.64
- Chrome 111.0.5563.*

## 文件结构
- AddMusic
  - main.py
  - onlineOptions
      - search.py 
  - localOptions
      - getTXT.py
      - formatTXT.py
  - common.py
  - log.txt
  - README.md

## 部分文件说明
- main.py为程序入口。
- formatTXT.py是自用脚本，可忽略。
- common.py里封装了Selenium自动化方法。
- log.txt是添加失败日志。


## 注意事项
1. 运行后需要使用QQ手动扫码登录。
2. 重复添加的歌曲不受影响。
3. 文件名格式

   标准音频文件名为'[歌名] [歌手]'，无视后缀。由于本脚本只检测第一个搜索结果，部分新获版权歌曲排名靠后故添加不了。

   1. 需要手动修改的类型有：
      - 无歌手
      - 歌手与QQ音乐官网不符（如飞儿乐团->F.I.R.飞儿乐团，外国歌手音译名）
      - 非正式版（如Live）
      - 歌名歌手颠倒
      - 文件夹下非音频的其他文件（需移出）

   2. 不支持的格式有：
      - 带空格的歌手/歌名
      - 部分特殊符号歌名
      - 无版权歌曲
      - 多个歌手


## 联系方式
主页：https://gitee.com/mignonhurricane
邮箱：ggmmio@163.com

> 爱倒腾，爱Python。欢迎技术交流。
