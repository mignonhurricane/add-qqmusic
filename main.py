# -*- encoding = UTF-8 -*-
"""
简介：程序入口，运行后根据提示输入、扫码即可。添加失败的记录在log.txt。

步骤：
1、读取本地文件
    1.5、修改读取的文件格式（自用）
2、打开网页搜索
"""
from onlineOptions.search import search1, search2, search3, search4, search5
from localOptions.formatTXT import trans1, trans2
from localOptions.getTXT import get0, get1, get2
from common import tearDown

# 1、读取文件，需要输入位置
get0()  # 步骤0：设置文件夹位置
get1()  # 步骤1：读取文件夹
get2()  # 步骤2：写入文件

# 1.5、修改格式（自用）
# trans1()  # 步骤1：读取、修改文件
# trans2()  # 步骤2：写入新文件

# 2、在线搜索，需要手动登录
search1()  # 步骤1：读取本地文件
search2()  # 步骤2：打开网页
search3()  # 步骤3：登录，等待手动扫码
search4()  # 步骤4：搜索歌曲、判断结果并添加收藏
tearDown()  # 关闭网页
search5()  # 步骤5：结果汇总并生成报告
