# -*- encoding = UTF-8 -*-
"""
简介：selenium控制通用方法。
主要是定位元素是否出现，否则退出程序，参数为元素xpath的key、等待n秒。
xpath的最后更新时间为2023/3/17。

功能列表：
- 打开网页 openURL
- 定位元素 locateElement
- 定位元素（等待） locateElementWait
- 悬浮在元素处 moveToElement
- 获取元素属性 getAttr
- 输入框清空并填充 inputBoxInit
- 关闭网页 tearDown
"""

import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

xpathE = {'loginButton': '//*[@id="app"]/div/div[1]/div/div[2]/span/a',
          'loginImg': '//*[@id="app"]/div/div[1]/div/div[2]/span/a/img[1]',
          'searchBox': '//*[@id="app"]/div/div[1]/div/div[1]/div[1]/input',
          'searchButton': '//*[@id="app"]/div/div[1]/div/div[1]/div[1]/button',
          'songList': '//*[@class="songlist__songname"]',
          'songName': '//*[@class="keyrender_default"]',
          'songArtist': '//*[@class="playlist__author"]',
          'songAlbum': '//*[@class="songlist__album"]/a',
          'optionList': '//*[@class="mod_list_menu"]',
          'optionAdd': '//*[@class="list_menu__item list_menu__add"]',
          'addILikeButton': '//*[@id="fav_pop"]/div/ul/li[1]/a'}
driver = webdriver.Chrome()


def openURL(url):
    driver.get(url)
    driver.maximize_window()


def locateElement(keywords, mode=0):
    if mode == 0:
        driver.find_element(by=By.XPATH, value=xpathE[keywords])
    elif mode == 1:
        driver.find_element(by=By.XPATH, value=xpathE[keywords]).click()
    return driver.find_element(by=By.XPATH, value=xpathE[keywords])


def locateElementWait(keywords, timeMax=7, forceExit=True):
    for t in range(timeMax):
        if t == timeMax - 1:
            print('没定位到%s' % keywords)
            if forceExit is True:
                exit()
            else:
                return False
        else:
            try:
                driver.find_element(by=By.XPATH, value=xpathE[keywords])
            except Exception:
                print(t)
            else:
                return True
            time.sleep(1)


# 0=移动，1=移动+点击
def moveToElement(keywords, mode=0):
    if mode == 0:
        ActionChains(driver).move_to_element(keywords).perform()
    elif mode == 1:
        ActionChains(driver).move_to_element(keywords).click().perform()


def getAttr(element, attribute):
    attr = element.get_attribute(attribute)
    return attr


def inputBoxInit(element, keywords):
    element.clear()
    element.send_keys(keywords)


def tearDown():
    driver.close()
    driver.service.stop()
