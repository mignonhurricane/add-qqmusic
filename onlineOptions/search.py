# -*- encoding = UTF-8 -*-
"""
简介：读取本地txt文件列表，添加到QQ音乐收藏（我喜欢）

步骤：
1、读取本地文件
2、打开网页
3、手动扫码登录
4、搜索、判断、添加
5、关闭网页
6、生成报告

"""
import time
from datetime import datetime
from common import locateElementWait, locateElement, openURL, moveToElement, getAttr, inputBoxInit
from localOptions.getTXT import pathGoal


fileName = pathGoal + '\musicList.txt'  # 目标列表文件名
url_qqMusic = 'https://y.qq.com/'  # QQ音乐官网
timeMax = 40  # 网页手动登录等待时长，单位：秒
songListLocal = []  # 目标列表，格式：['歌名1 歌手','歌名2 歌手',...]
songListSuccess = []  # 已添加歌曲清单
songListFail = [[], [], [], []]  # 缺失歌曲清单，list[0、1、2]分别为无同名歌曲、无该歌手版本、无正规版
songListFailResult = []  # 缺失歌曲搜索结果，格式为['歌名1 歌手 专辑',...]


# 步骤1：读取本地文件
def search1():
    try:
        with open(fileName, mode='r', encoding='UTF-8') as file:
            txtList = file.readlines()
            for i in txtList:
                songListLocal.append(i.rstrip('\n'))
            print('即将搜索以下歌曲：\n')
            print(songListLocal)
    except Exception as e:
        print(e)
    finally:
        file.close()


# 步骤2：打开网页
def search2():
    openURL(url_qqMusic)
    time.sleep(1)


# 步骤3：登录，等待手动扫码
def search3():
    print('等待手动扫码登录，%d秒计时开始' % timeMax)
    locateElement('loginButton', mode=1)
    locateElementWait('loginImg', timeMax=timeMax)
    print('已登录')


# 步骤4：搜索歌曲、判断结果并添加收藏
def search4():
    for words in songListLocal:
        # 输入并搜索
        inputBoxInit(locateElement('searchBox'), words)
        print('正在搜索%s' % words)
        searchButton = locateElement('searchButton')
        searchButton.click()
        time.sleep(1)
        locateElementWait('songName')
        locateElementWait('songArtist')
        locateElementWait('songAlbum')

        # 判断搜索结果
        songName = getAttr(locateElement('songName'), 'title')  # 搜索结果，歌名
        songArtist = locateElement('songArtist').text  # 搜索结果，歌手
        songAlbum = locateElement('songAlbum').text  # 搜索结果，专辑
        songResult = songName + ' ' + songArtist + ' ' + songAlbum  # 完整搜索结果，格式：'歌名 歌手 专辑'，用于统计缺失歌曲
        w1 = words.rsplit(' ', 1)[0]  # 目标第1部分，歌名
        w2 = words.rsplit(' ', 1)[1]  # 目标第2部分，歌手
        if songName == w1:
            if songArtist == w2:
                print('1 已定位到正确版本的【%s】' % words)
                # 正常流程
                moveToElement(locateElement('songList'))
                time.sleep(0.5)
                locateElementWait('optionAdd', timeMax=3)
                moveToElement(locateElement('optionAdd'), mode=1)
                time.sleep(0.5)
                locateElementWait('addILikeButton', timeMax=3)
                locateElement('addILikeButton', mode=1)
                songListSuccess.append(words)
            else:
                print('0 【%s】无%s该歌手版本，第一条搜索结果为%s' % (w1, w2, songResult))
                songListFail[1].append(words)
                songListFailResult.append(songResult)
                continue
        else:
            if ('Live' or 'live' or '版)' or '版）') in songName:
                print('0 【%s】没有正规版，第一条搜索结果为%s' % (w1, songResult))
                songListFail[2].append(words)
                songListFailResult.append(songResult)
                continue
            else:
                print('0 【%s】没有同名歌曲，第一条搜索结果为%s' % (w1, songResult))
                songListFail[0].append(words)
                songListFailResult.append(songResult)
                continue


# 步骤5：结果汇总并生成报告
def search5():
    print('————————————————————————————————————————————————')
    songListFailCount = len(songListFail[0]) + len(songListFail[1]) + len(songListFail[2])
    print('1、已添加歌曲共%d首，分别为：\n%s\n' % (len(songListSuccess), str(songListSuccess)))
    print('2、不符合歌曲共%d首，无同名歌、无该歌手版、无正规版分别为：' % songListFailCount)
    print(songListFail[0])
    print(songListFail[1])
    print(songListFail[2])
    print(songListFailResult)
    print(datetime.now())

    with open('../log.txt', mode='a', encoding='UTF-8') as file:
        file.writelines('————————————————————————————————————————————————\n')
        file.writelines(str(datetime.now()))
        file.writelines('\n\n本次添加失败数量%d/总数%d：\n' % (len(songListFailResult), len(songListLocal)))
        file.writelines('\nsongListFail:\n%s\n' % str(songListFail))
        file.writelines('\nsongListFailResult:\n%s\n' % str(songListFailResult))
        file.writelines('————————————————————————————————————————————————\n')
        file.close()
