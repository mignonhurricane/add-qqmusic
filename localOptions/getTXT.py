# -*- encoding = UTF-8 -*-
"""
简介：读取本地文件夹下的所有文件并生成txt文件。

步骤：
0、设置文件夹位置（扩展）
1、读取文件夹
2、写入文件

需要注以下几点：
    1. 需要手动修改的类型有：
      - 无歌手
      - 歌手与QQ音乐官网不符（如飞儿乐团->F.I.R.飞儿乐团，外国歌手音译名）
      - 非正式版（如Live）
      - 歌名歌手颠倒
      - 文件夹下非音频的其他文件（需移出）

    2. 不支持的格式有：
      - 带空格的歌手/歌名
      - 部分特殊符号歌名
      - 无版权歌曲
"""
import os

pathSource = r'F:'  # 磁盘中歌曲文件所在文件夹
pathGoal = r'D:\PyCharm\AddMusic'  # 写入歌曲列表文件的文件夹
list1 = []  # 写入文件的歌曲列表，不含扩展名
musicListFile = pathGoal + '\musicList.txt'  # 生成的歌曲清单txt文件。使用formatTXT需修改格式为musicList0.txt


# 0、设置文件夹位置（扩展）
def get0():
    global pathSource, pathGoal
    pathSource = input('请输入歌曲文件所在文件夹：')
    pathGoal = input('请输入另一个用于输出txt文件的文件夹：')


# 1、获取文件列表
def get1():
    list0 = os.listdir(pathSource)  # 初步读取的歌曲列表，含扩展名
    for l0 in list0:
        l0 = l0.rsplit('.', 1)[0]
        list1.append(l0)  # 歌曲列表，不含扩展名
        print('已读取歌曲：%s' % str(l0))


# 2、覆盖式将列表写入文件
def get2():
    with open(musicListFile, mode='w', encoding='UTF-8') as file:
        for l1 in list1:
            file.writelines(l1 + '\n')
            print('已写入歌曲：%s' % str(l1))
