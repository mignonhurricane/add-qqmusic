# -*- encoding = UTF-8 -*-
"""
简介：将特定文本格式为'[歌名] [歌手]'。

步骤：
1、读取、修改文件
    1. 读取文件
    2. 去除-前后的空格，优化为'[歌手]-[歌名]'
    3. 歌手优化，Alin、->A-Lin，F.I.R、飞儿乐团->F.I.R.飞儿乐团，
    4. 颠倒并连接，优化为'[歌名] [歌手]'
2、写入新文件
"""

file1 = 'musicList0.txt'  # 未格式化的文件
file2 = 'musicList.txt'  # 已格式化的文件
list1 = []  # 每行结果
list2 = []  # 格式化后结果


# 1、读取、修改文件
def trans1():
    with open(file1, mode='r', encoding='UTF-8') as file:
        # 1. 读取文件
        listLines = file.readlines()
        # 2. 去掉连接符及前后的空格（用-分割、去掉空格）
        for lLines in listLines:
            words1 = lLines.rsplit('-', 1)
            words1[0] = words1[0].strip()  # 歌手
            words1[1] = words1[1].strip()  # 歌名
            # 3. 优化歌手
            if 'Alin' in words1[0]:
                words1[0] = words1[0].replace('Alin', 'A-Lin')
            elif 'F.I.R' in words1[0]:
                words1[0] = words1[0].replace('F.I.R', 'F.I.R.飞儿乐团')
            elif '飞儿乐团' in words1[0]:
                words1[0] = words1[0].replace('飞儿乐团', 'F.I.R.飞儿乐团')
            # 4. 颠倒歌手与歌名（倒叙连接）
            words2 = words1[1] + ' ' + words1[0]
            list2.append(words2)
        file.close()


# 2、写入新文件
def trans2():
    with open(file2, mode='w', encoding='UTF-8') as file:
        for l2 in list2:
            file.writelines(l2 + '\n')
        file.close()
